
public class SplayTrees {
	static Boolean found;
	SplayTreeNode<Integer> zig(SplayTreeNode<Integer> tree, Boolean isLeft){
		if(isLeft){
			tree = rotateRight(tree);
		}else{
			tree =rotateLeft(tree);
		}
		
		return tree;
	}
	SplayTreeNode<Integer> zig2(SplayTreeNode<Integer> tree, Boolean isLeft){
		if(isLeft){
			tree = rotateRight(tree.parent);
			tree = rotateRight(tree.left);
		}else{
			tree = rotateLeft(tree.parent);
			tree = rotateLeft(tree.right);
		}
		
		return tree;
	}
	SplayTreeNode<Integer> zigzag(SplayTreeNode<Integer> tree, Boolean isLeft){
		if(isLeft){
			tree = rotateRight(tree);
			tree.parent.right = tree;
			tree = rotateLeft(tree);
		}else{
			tree = rotateLeft(tree);
			tree.parent.left = tree;
			tree = rotateRight(tree);
		}
		return tree;
	}
	
	
	SplayTreeNode<Integer> insert(SplayTreeNode<Integer> tree, Integer x){
		Boolean isLeft = true;
		if(tree==null && tree==null){
			tree = new SplayTreeNode<Integer>();
			tree.data = x;
			tree.parent = null;
			tree.left = null;
			tree.right = null;
			return tree;
		}
		else{
			SplayTreeNode<Integer> p = tree;
			SplayTreeNode<Integer> prev = tree;
			while(p!=null){
				prev = p;
				if(Integer.compare(p.data, x) >= 0 ){
					p = p.left;
					isLeft=true;
				}else{
					p = p.right;
					isLeft=false;
				}
			}
			//Insert a new element
			if(isLeft){
				prev.left = new SplayTreeNode<Integer>();
				prev.left.data = x;
				prev.left.parent = prev;
				
				p = prev.left;
			}else{
				prev.right = new SplayTreeNode<Integer>();
				prev.right.data = x;
				prev.right.parent = prev;
				
				p = prev.right;
			}
			while(p.parent!=null){
				p = splay(p);	
			}
			return p;
		}	
	}
	
	
	void print(SplayTreeNode<Integer> top){
		if(top!=null){
			System.out.print(top.data+" ");
			print(top.left);
			print(top.right);
		}
	}
	SplayTreeNode<Integer> remove(SplayTreeNode<Integer> tree, Integer x){
		return null;
	}
	SplayTreeNode<Integer> find(SplayTreeNode<Integer> tree, Integer x){
		SplayTreeNode<Integer> temp = tree;
		SplayTreeNode<Integer> prev = tree;
		SplayTreeNode<Integer> y;
		found = false;
		//Find the node just as you would in BST
		while(temp!=null){
			if(Integer.compare(temp.data, x)>0){
				prev = temp;
				temp = temp.left;
			}else if (Integer.compare(temp.data, x) < 0){
				prev = temp;
				temp = temp.right;
			}else if(Integer.compare(temp.data, x)==0){
				found = true;
				break;
			}
		}		
		if(found){	
			y = splay(temp);
		}else{
			y = splay(prev);
		}
		
		
		while(y.parent!=null){
			y = splay(y);
		}
		return y;
	}
	private SplayTreeNode<Integer> splay(SplayTreeNode<Integer> temp) {
		// TODO Auto-generated method stub
		if(temp.parent==null)
			return temp;
		SplayTreeNode<Integer> y = null;
		SplayTreeNode<Integer> G = null;
		//ZIG
		SplayTreeNode<Integer> P = temp.parent;
		if(P.parent==null && P.left == temp){
			y = zig(temp,true);
		}else if(P.parent==null && P.right == temp){
			y = zig(temp,false);
		}
		
		if(P.parent!=null){
			//Grandparent exists
			G = P.parent;
			//ZIG-ZIG
			if(P.left==temp && P==G.left){
				y = zig2(temp,true);
			}else if(P.right==temp && P==G.right){
				y = zig2(temp,false);
			}else if(P.right == temp && G.left == P){//ZIG-ZAG
				y = zigzag(temp,false);
			}else if(P.left == temp && G.right==P){
				y = zigzag(temp,true);
			}

			return y;
		}
	
		return y;
	}
	private SplayTreeNode<Integer> rotateLeft(SplayTreeNode<Integer> tree) {
		// TODO Auto-generated method stub
		SplayTreeNode<Integer> temp;
		SplayTreeNode<Integer> temp1;
		if(tree.parent.parent!=null){
			temp1 = tree.parent.parent;
		}else{
			temp1 = null;
		}
		temp = tree.left;
		tree.left = tree.parent;
		tree.left.parent = tree;
		tree.left.right = temp;
		if(temp!=null)
			temp.parent = tree.left;
		
		tree.parent = temp1;
		if(temp1!=null){
			if(tree.parent.left==tree.left){
				tree.parent.left = tree;
			}else{
				tree.parent.right = tree;
			}
		}
		return tree;
	}
	private SplayTreeNode<Integer> rotateRight(SplayTreeNode<Integer> tree) {
		// TODO Auto-generated method stub
		SplayTreeNode<Integer> temp;
		SplayTreeNode<Integer> temp1;
		if(tree.parent.parent!=null){
			temp1 = tree.parent.parent;
		}else{
			temp1 = null;
		}
		temp = tree.right;
		tree.right = tree.parent;
		tree.right.parent = tree;
		tree.right.left = temp;
		if(temp!=null)
			temp.parent = tree.right;
		tree.parent = temp1;
		if(temp1!=null){
			
			if(tree.parent.right==tree.right){
				tree.parent.right = tree;
			}else{
				tree.parent.left = tree;
			}
		}
		return tree;
	}
}
